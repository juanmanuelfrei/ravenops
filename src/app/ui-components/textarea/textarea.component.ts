import { Component, Input, EventEmitter, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'rvn-textarea',
  standalone: true,
  imports: [CommonModule],
  template: `
    <label [for]="name">{{ labelText }}</label>
    <textarea
      [disabled]="disabled"
      [placeholder]="placeholder"
      [ariaLabel]="name"
      [id]="name"
      [name]="name"
      (keyup)="emitValue($event)"
      [ngStyle]="{ 'max-height.px': maxHeight, 'height.px': initialHeight }"
    ></textarea>
  `,
})
export class TextareaComponent {
  @Input() name!: string;
  @Input() labelText!: string;
  @Input() placeholder: string = '';
  @Input() disabled: boolean = false;
  @Input() maxHeight: number = 240;
  @Input() initialHeight: number = 150;
  @Output() inputValue = new EventEmitter<string>();

  emitValue(event: KeyboardEvent) {
    const input = event.target as HTMLTextAreaElement;
    this.inputValue.emit(input.value);
  }
}
