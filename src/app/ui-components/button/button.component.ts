import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'rvn-button',
  standalone: true,
  imports: [CommonModule],
  template: `
    <button
      [ngClass]="[kind, size]"
      [class.fullWidth]="fullWidth"
      (click)="clicked.emit()"
      [type]="type"
      [disabled]="disabled"
    >
      {{ description }}
    </button>
  `,
})
export class ButtonComponent {
  @Input() type: 'submit' | 'button' = 'button';
  @Input() description: string = '';
  @Input() fullWidth: boolean = false;
  @Input() disabled: boolean = false;
  @Input() kind: 'primary' | 'secondary' | 'third' = 'primary';
  @Input() size: 'sm' | 'md' | 'lg' = 'md';

  @Output() clicked = new EventEmitter<void>();
}
