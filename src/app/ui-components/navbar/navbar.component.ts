import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule],
  template: `
    <header>
      <nav>
        <p>Raven Ops</p>
        <ul>
          <li>Contact</li>
          <li>About Us</li>
          <li>Clients</li>
          <li>Our service</li>
        </ul>
      </nav>
    </header>
  `,
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent {}

// quienes somos
// 			servicios - Mostrar los servicios que brindamos
// 			clientes
// 			contactos - formulario de contacto clientes@raven.com

// 			mostrar info de los servicios

// 			agregar idiomas - Ingles/Esp

// 			linkedin
// 			ig
// 			mail

// 			Nombre/dominio :
// 			Raven
