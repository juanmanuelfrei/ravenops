import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from '../navbar/navbar.component';
import { HomeComponent } from 'src/app/pages/home/home.component';
import { FooterComponent } from '../footer/footer.component';

const mainLayoutImports = [
  NavbarComponent,
  HomeComponent,
  FooterComponent,
  CommonModule,
];

@Component({
  selector: 'rvn-main-layout',
  standalone: true,
  imports: mainLayoutImports,
  template: `
    <app-navbar />
    <main>
      <app-home />
    </main>
    <app-footer />
  `,
})
export class MainLayoutComponent {}
