import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule],
  template: `
    <footer>
      <p>
        Raven Ops 2023 - All rights reserved
      </p>
    </footer>
  `,
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  
}
