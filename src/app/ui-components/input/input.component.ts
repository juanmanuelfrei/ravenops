import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'rvn-input',
  standalone: true,
  imports: [CommonModule],
  template: `
    <label [for]="name">{{ labelText }}</label>
    <input
      (keyup)="emitValue($event)"
      [ariaLabel]="name"
      [placeholder]="placeholder"
      [type]="type"
      [disabled]="disabled"
      [id]="name"
      [name]="name"
    />
  `,
})
export class InputComponent {
  @Input() type: 'text' | 'password' | 'email' = 'text';
  @Input() name!: string;
  @Input() labelText!: string;
  @Input() placeholder: string = '';
  @Input() disabled: boolean = false;
  @Output() inputValue = new EventEmitter<string>();

  emitValue(event: KeyboardEvent) {
    const input = event.target as HTMLInputElement;
    this.inputValue.emit(input.value);
  }
}
