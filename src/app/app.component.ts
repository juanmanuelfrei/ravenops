import { Component } from '@angular/core';
import { MainLayoutComponent } from './ui-components/main-layout/main-layout.component';

@Component({
  selector: 'app-root',
  template: `<rvn-main-layout />`,
  standalone: true,
  imports: [MainLayoutComponent],
})
export class AppComponent {}
