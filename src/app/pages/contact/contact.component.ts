import { Component, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from 'src/app/ui-components/button/button.component';
import { InputComponent } from 'src/app/ui-components/input/input.component';
import { TextareaComponent } from 'src/app/ui-components/textarea/textarea.component';
import {
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'rvn-contact',
  standalone: true,
  imports: [
    CommonModule,
    ButtonComponent,
    InputComponent,
    TextareaComponent,
    ReactiveFormsModule,
  ],
  // imports: [CommonModule, ButtonComponent, InputComponent, TextareaComponent],
  template: `
    <div class="contact">
      <h1>Contact</h1>
      <form [formGroup]="formularioPrueba" autocomplete="off">
        <div class="form-group">
          <rvn-input labelText="Name" name="name" />
        </div>
        <div class="form-group">
          <rvn-input labelText="Email" name="email" type="email" />
        </div>
        <div class="form-group">
          <rvn-textarea
            name="description"
            placeholder="Send us your requiremnet and we will contact you to proceded."
          />
        </div>
        <rvn-button size="lg" description="Send" />
      </form>
    </div>
  `,
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent {
  private fb = inject(FormBuilder);
  formularioPrueba: FormGroup = this.fb.group({
    nombre: ['', Validators.required],
    email: ['', Validators.required],
  });
}
