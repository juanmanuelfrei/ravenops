import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-clients',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      clients works!
    </p>
  `,
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent {

}
