import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-about-us',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      about-us works!
    </p>
  `,
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent {

}
