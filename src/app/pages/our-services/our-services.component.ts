import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-our-services',
  standalone: true,
  imports: [CommonModule],
  template: `
    <p>
      our-services works!
    </p>
  `,
  styleUrls: ['./our-services.component.scss']
})
export class OurServicesComponent {

}
