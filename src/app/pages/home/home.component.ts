import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from '../contact/contact.component';
import { ClientsComponent } from '../clients/clients.component';
import { AboutUsComponent } from '../about-us/about-us.component';
import { OurServicesComponent } from '../our-services/our-services.component';

const components = [
  ContactComponent,
  ClientsComponent,
  AboutUsComponent,
  OurServicesComponent,
];

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, components],
  template: `
    <section class="main-message">
      <h1>Seguridad Informática Confiable</h1>
      <p>home works!</p>
      <app-our-services />
      <app-clients />
      <app-about-us />
      <rvn-contact />
    </section>
  `,
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {}

// "Tu seguridad informática en buenas manos." (Your Cybersecurity in Good Hands)
// "Protegiendo lo que más importa: tus datos." (Protecting What Matters Most: Your Data)
// "Innovación en ciberseguridad a tu servicio." (Innovation in Cybersecurity at Your Service)
// "Defendiendo tu mundo digital, sin compromisos." (Defending Your Digital World, Uncompromisingly)
// "Soluciones de seguridad informática de vanguardia." (Cutting-Edge Cybersecurity Solutions)
// "La tranquilidad de saber que estás protegido." (Peace of Mind, Knowing You're Protected)
// "Ciberseguridad eficaz para desafíos actuales." (Effective Cybersecurity for Today's Challenges)
// "Tu socio de confianza en seguridad cibernética." (Your Trusted Partner in Cybersecurity)
// "Manteniendo tus sistemas a salvo, siempre." (Keeping Your Systems Safe, Always)
// "Nuestra misión: tu seguridad en línea." (Our Mission: Your Online Security)

/*

general 


    <main>
        <section id="inicio">
            <h1>Página de Inicio</h1>
            <!-- Contenido de la página de inicio -->
        </section>

        <section id="acerca-de">
            <h1>Acerca de Nosotros</h1>
            <!-- Contenido de la sección "Acerca de Nosotros" -->
        </section>

        <section id="servicios">
            <h1>Nuestros Servicios</h1>
            <!-- Contenido de la sección "Nuestros Servicios" -->
        </section>

        <section id="contacto">
            <h1>Contacto</h1>
            <!-- Formulario de contacto u otra información de contacto -->
        </section>
    </main>



Dentro de un section
<section id="acerca-de">
    <h1>Acerca de Nosotros</h1>

    <article>
        <h2>Nuestra Historia</h2>
        <p>Descripción de la historia y el origen de la empresa.</p>
    </article>

    <article>
        <h2>Nuestro Equipo</h2>
        <p>Información sobre los miembros del equipo, sus roles y experiencia.</p>
    </article>

    <article>
        <h2>Nuestra Misión y Valores</h2>
        <p>Explicación de la misión y los valores de la empresa.</p>
    </article>
</section>



*/
